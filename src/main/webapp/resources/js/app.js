(function(){
    'use strict';

    angular
        .module('app',['ngRoute','app.controllers','app.services','app.directives'])
        .config(routeConfig)
        .run(run);

    routeConfig.$inject = ['$routeProvider'];
    run.$inject = ['$rootScope','utils'];

    function routeConfig($routeProvider){
        $routeProvider.
            when('/teachers', {
                templateUrl: 'resources/partials/teachers.html',
                controller: 'TeachersController',
                controllerAs: 'vm',
                resolve: {
                    data: function ($q,DataService) {
                        return $q.all({
                            teachers: DataService('teachers').getAll().$promise,
                            subjects: DataService('subjects').getAll().$promise
                        })
                    }
                }
            })
            .when('/subjects',{
                templateUrl: 'resources/partials/subjects.html',
                controller: 'SubjectsController',
                controllerAs: 'vm',
                resolve: {
                    data: function (DataService) {
                        return DataService('subjects').getAll().$promise
                    }
                }
            })
            .otherwise({
                redirectTo: '/'
            });
    }

    function run($rootScope, utils){
        $rootScope.$on('$routeChangeStart', function (event, current, previous) {
            if (current.$$route && current.$$route.resolve) {
                $rootScope.loading = true;
            }
        });

        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            $rootScope.alert = {};
            $rootScope.loading = false;
        });

        $rootScope.$on('$routeChangeError', function (event, current, previous, rejection) {
            $rootScope.alert = utils.getErrorAlert(rejection);
            $rootScope.loading = false;
        });

    }
})();

(function(){
    'use strict';

    angular
        .module('app.services',['ngResource'])
        .service('DataService',DataService)
        .factory('utils',utils)
        .service('MockDataService',MockDataService);

    DataService.$inject = ['$resource'];

    function utils(){
        return {
            'getErrorAlert': function(error){
                var errMsg = error.data ? error.data.desc : error.statusText;
                return {
                    type: 'alert-danger',
                    msg: 'Ошибка! ('+ errMsg + ')'
                };
            }
        }
    }

    function DataService($resource){
        var actions = {};

        actions.teachers = {
            'getAll': { method: 'GET', url: '/teachers', isArray: true },
            'getById': { method: 'GET',params:{id: '@id'}, url: '/teachers/:id' },
            'save': { method: 'POST', url: '/teachers' },
            'addSubject': { method: 'POST',params:{id: '@id'}, url: '/teachers/:id/addSubject' },
            'removeSubject': { method: 'POST',params:{id: '@id'}, url: '/teachers/:id/removeSubject' },
            'remove': { method: 'DELETE',params:{id: '@id'},url: '/teachers/:id' }
        };

        actions.subjects = {
            'getAll': { method: 'GET', url: '/subjects',isArray: true },
            'getById': { method: 'GET',params:{id: '@id'}, url: '/subjects/:id' },
            'save': { method: 'POST', url: '/subjects' },
            'remove': { method: 'DELETE',params:{id: '@id'},url: '/subjects/:id' }
        };

        return function(sourceName){
            return $resource('', {}, actions[sourceName]);
        }
    }

    function MockDataService(){
        var data = {};

        data.subjects = [
            {id:1, name:"Информатика"},
            {id:2, name:"Биология"},
            {id:3, name:"Георгафия"},
            {id:4, name:"Математика"}
        ];

        data.teachers = [
            {id:1,lastName:"Сидоров", firstName:"Иван", middleName: "Петрович"},
            {id:2,lastName:"Иванов", firstName:"Петр", middleName: "Васильевич",
                subjects: [data.subjects[0],data.subjects[2]] },
            {id:3,lastName:"Петров", firstName:"Василий", middleName: "Сидорович"},
            {id:4,lastName:"Петров", firstName:"Андрей", middleName: "Викторович"}
        ];

        return function(name){
            return{
                getAll: function(){
                    return data[name];
                },
                getById: function(id){
                    return data[name].filter(function(item){
                        return item.id === id;
                    })[0];
                },
                add: function(item){
                    data[name].push(item);
                },
                save: function(id, updated){
                    var found = this.getById(id);
                    if(found) found = angular.copy(updated);
                },
                addSubject: function(teacherId,subject){
                    var t = this.getById(teacherId);
                    if(!t.subjects)t.subjects = [];
                    t.subjects.push(subject);
                },
                removeSubject: function(teacherId,subject){
                    var t = this.getById(teacherId);
                    if(t.subjects)
                        t.subjects.splice(t.subjects.indexOf(subject),1);
                },
                remove: function(id){
                    var t = this.getById(id);
                    if (t) data[name].splice(data[name].indexOf(t),1);
                }
            }
        }
    }

})();

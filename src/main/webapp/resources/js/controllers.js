(function(){
    'use strict';

    angular
        .module('app.controllers',[])
        .controller('TeachersController',TeachersController)
        .controller('SubjectsController',SubjectsController);

    TeachersController.$inject = ['$rootScope','DataService','data','utils'];
    TeachersController.$inject = ['$rootScope','DataService','data','utils'];

    function TeachersController($rootScope, DataService, data, utils){
        var vm = this;
        var teacherData = DataService('teachers');

        activate();

        function activate(){
            vm.title = 'Список преподавателей';
            initObjects();
            vm.data = data;
        }

        function initObjects(){
            vm.editMode = {};
            vm.selected = {};
        }

        vm.reset = function(){
            initObjects();
            vm.addMode = false;
        };

        function errorHandler(error){
            $rootScope.alert = utils.getErrorAlert(error);
        }

        vm.saveSubject = function(teacher){
            $rootScope.alert = {};
            teacherData.addSubject({id:teacher.id},vm.selected.subject.id, function(){
                teacher.subjects.unshift(vm.selected.subject);
            },errorHandler);
        };

        vm.addSubject = function(teacherId){
            $rootScope.alert = {};
            vm.reset();
            vm.editMode.teacherId = teacherId;
            vm.editMode.isNewSubject = true;
        };

        vm.isAddSubject = function(teacherId){
            return vm.editMode.teacherId === teacherId && vm.editMode.isNewSubject;
        };

        vm.isEditMode = function(teacherId){
            return vm.editMode.teacherId === teacherId && !vm.editMode.isNewSubject;
        };

        vm.removeSubject = function(teacher, subject){
            $rootScope.alert = {};
            teacherData.removeSubject({id:teacher.id}, subject.id, function(){
                teacher.subjects.splice(teacher.subjects.indexOf(subject),1);
            }, errorHandler);
        };

        vm.removeTeacher = function(teacher){
            $rootScope.alert = {};
            teacherData.remove({id: teacher.id}, function(){
                vm.data.teachers.splice(vm.data.teachers.indexOf(teacher),1);
            },errorHandler);
        };

        vm.editTeacher = function(teacherId){
            $rootScope.alert = {};
            teacherData.getById({id: teacherId}, function(resp){
                vm.editMode.teacherId = resp.id;
                vm.editMode.isNewSubject = false;
                vm.selected.teacher = resp;
            }, errorHandler);
        };

        vm.addTeacher = function(){
            $rootScope.alert = {};
            vm.addMode = true;
        };

        vm.saveTeacher = function(){
            $rootScope.alert = {};
            var savingTeacher = vm.selected.teacher;
            teacherData.save(savingTeacher, function(resp){
                var id = savingTeacher.id;
                if(id){
                    var foundTeacher = vm.data.teachers.filter(function(teacher){
                        return teacher.id === id;
                    })[0];
                    vm.data.teachers[vm.data.teachers.indexOf(foundTeacher)] = savingTeacher;
                }else{
                    if(!vm.data.teachers) vm.data.teachers = [];
                    resp.subjects = [];
                    vm.data.teachers.push(resp);
                }
            }, errorHandler);

            vm.reset();
        };
    }

    function SubjectsController($rootScope, DataService, data, utils){
        var vm = this;
        var subjectData = DataService('subjects');

        activate();

        function activate(){
            vm.title = 'Список предметов';
            vm.editableSubject = {};
            vm.subjects = data;
        }

        function errorHandler(error){
            $rootScope.alert = utils.getErrorAlert(error);
        }

        vm.add = function(){
            $rootScope.alert = {};
            vm.editableSubject.name = '';
            vm.subjects.unshift(vm.editableSubject);
        };

        vm.remove = function(subject){
            $rootScope.alert = {};
            subjectData.remove({id: subject.id}, function(){
                vm.subjects.splice(vm.subjects.indexOf(subject),1);
            },errorHandler);
        };

        vm.edit = function(subjectId){
            $rootScope.alert = {};
            subjectData.getById({id: subjectId}, function(resp){
                vm.editableSubject = resp;
            }, errorHandler);
        };

        vm.isEditMode = function(subject){
            return subject.id ? vm.editableSubject.id === subject.id : true;
        };

        vm.reset = function(){
            if(!vm.editableSubject.id)
                vm.subjects.splice(vm.subjects.indexOf(vm.editableSubject),1);
            vm.editableSubject = {};
        };

        vm.save = function(){
            $rootScope.alert = {};
            if(!vm.editableSubject || !vm.editableSubject.name){
               $rootScope.alert = {
                   type: 'alert-danger',
                   msg: 'Ошибка! Не заполнено наименование предмета'
                };
                return;
            }

            var savingSubject = vm.editableSubject;
            subjectData.save(savingSubject, function(resp){
                var id = savingSubject.id;
                if(id){
                    var foundSubject = vm.subjects.filter(function(subject){
                        return subject.id === id;
                    })[0];
                    vm.subjects[vm.subjects.indexOf(foundSubject)] = savingSubject;
                }else{
                    if(!vm.subjects) vm.subjects = [];
                    vm.subjects.push(resp);
                }
            }, errorHandler);
            vm.reset();
        };
    }
})();

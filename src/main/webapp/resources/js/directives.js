(function(){
    'use strict';

    angular
        .module('app.directives',[])
        .directive('dirFormEditTeacher',dirFormEditTeacher);

    function dirFormEditTeacher(){
        return {
            restrict: 'E',
            replace:true,
            templateUrl: 'resources/partials/form-edit-teacher.html'
        };
    }

})();

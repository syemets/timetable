package test.timetable.app.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import test.timetable.app.model.ErrorMessage;
import test.timetable.app.model.Subject;
import test.timetable.app.services.SubjectService;

import java.util.List;

@Controller
@RequestMapping("/subjects")
public class SubjectController {

    Logger LOGGER = Logger.getLogger(SubjectController.class);

    @Autowired
    SubjectService subjectService;

    // GET: /subjects
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public List<Subject> getAll() {
        return subjectService.findAllSubjects();
    }

    // GET: /subjects/5
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public Subject get(@PathVariable("id") long id) {
        return subjectService.findSubjectById(id);
    }

    // POST: /subjects
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public Subject save(@RequestBody Subject subject) {
        return subjectService.saveSubject(subject);
    }

    // DELETE: /subjects/5
    @ResponseBody
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public void remove(@PathVariable("id") long id) {
        subjectService.deleteSubject(id);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage());
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ErrorMessage error = new ErrorMessage(status.value(), exc.getMessage());
        return new ResponseEntity<>(error, status);
    }
}

package test.timetable.app.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import test.timetable.app.model.ErrorMessage;
import test.timetable.app.services.ConflictSaveEntityException;
import test.timetable.app.model.Teacher;
import test.timetable.app.model.TeacherSubject;
import test.timetable.app.services.TeacherService;

import java.util.List;

@Controller
@RequestMapping("/teachers")
public class TeacherController {

    Logger LOGGER = Logger.getLogger(TeacherController.class);

    @Autowired
    TeacherService teacherService;

    // GET: /teachers
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public List<Teacher> getAll() {
        return teacherService.findAllTeachers();
    }

    // GET: /teachers/5
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public Teacher get(@PathVariable("id") long id) {
        return teacherService.findTeacherById(id);
    }


    // POST: /teachers
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public Teacher save(@RequestBody Teacher teacher) {
        return teacherService.saveTeacher(teacher);
    }

    // POST: /teachers/5/addSubject
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{id}/addSubject",method = RequestMethod.POST)
    public TeacherSubject saveTeacherSubject(@PathVariable("id") long teacherId, @RequestBody long subjectId) {
        return teacherService.saveTeacherSubject(teacherId, subjectId);
    }

    // DELETE: /teachers/5
    @ResponseBody
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable("id") long id) {
        teacherService.deleteTeacher(id);
    }

    // POST: /teachers/5/removeSubject
    @ResponseBody
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}/removeSubject",method = RequestMethod.POST)
    public void removeTeacherSubject(@PathVariable("id") long teacherId, @RequestBody long subjectId) {
        teacherService.deleteTeacherSubject(teacherId, subjectId);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage());

        HttpStatus status = HttpStatus.BAD_REQUEST;
        if (exc.getClass() == ConflictSaveEntityException.class)
            status = HttpStatus.CONFLICT;

        ErrorMessage error = new ErrorMessage(status.value(), exc.getMessage());
        return new ResponseEntity<>(error, status);
    }
}

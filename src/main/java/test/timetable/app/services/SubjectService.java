package test.timetable.app.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.timetable.app.dao.SubjectRepository;
import test.timetable.app.model.Subject;

import java.util.List;

import static org.springframework.util.Assert.notNull;
import static test.timetable.app.services.ValidationUtils.assertNotBlank;

@Service
public class SubjectService {
    @Autowired
    private SubjectRepository subjectRepository;

    @Transactional(readOnly = true)
    public List<Subject> findAllSubjects() {
        return subjectRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Subject findSubjectById(Long id) {
        Subject subject = subjectRepository.findById(id);
        notNull(subject, "subject by id=" + id + " is not fount");
        return subject;
    }

    @Transactional
    public Subject saveSubject(Subject subject) {

        assertNotBlank(subject.getName(), "name cannot be blank");

        Subject result;
        Long id = subject.getId();
        if (id != null) {
            result = subjectRepository.findById(id);
            notNull(result, "subject by id=" + id + " is not fount");
            result.setName(subject.getName());
        } else {
            result = subjectRepository.save(new Subject(subject.getName()));
        }

        return result;
    }

    @Transactional
    public void deleteSubject(long id){
        Subject subject = subjectRepository.findById(id);
        notNull(subject, "subject by id=" + id + " is not fount");
        subjectRepository.delete(subject);
    }
}

package test.timetable.app.services;


import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

public final class ValidationUtils {

    private ValidationUtils() {
        throw new NotImplementedException("Utility classes cannot be instantiated");
    }

    public static void assertNotBlank(String username, String message) {
        if (StringUtils.isBlank(username)) {
            throw new IllegalArgumentException(message);
        }
    }
}

package test.timetable.app.services;


public class ConflictSaveEntityException extends RuntimeException{

    public ConflictSaveEntityException() {
    }

    public ConflictSaveEntityException(String message) {
        super(message);
    }
}

package test.timetable.app.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.timetable.app.dao.SubjectRepository;
import test.timetable.app.dao.TeacherRepository;
import test.timetable.app.model.Subject;
import test.timetable.app.model.Teacher;
import test.timetable.app.model.TeacherSubject;

import java.util.List;

import static org.springframework.util.Assert.notNull;
import static test.timetable.app.services.ValidationUtils.assertNotBlank;

@Service
public class TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Transactional(readOnly = true)
    public List<Teacher> findAllTeachers() {
        return teacherRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Teacher findTeacherById(Long id) {
        Teacher teacher = teacherRepository.findById(id);
        notNull(teacher, "teacher by id=" + id + " is not fount");
        return teacher;
    }

    @Transactional
    public Teacher saveTeacher(Teacher teacher) {

        assertNotBlank(teacher.getLastName(), "last name cannot be blank");
        assertNotBlank(teacher.getFirstName(), "first name cannot be blank");
        assertNotBlank(teacher.getMiddleName(), "middle name cannot be blank");

        Teacher result;
        Long id = teacher.getId();
        if (id != null) {
            result = teacherRepository.findById(id);
            notNull(result, "teacher by id=" + id + " is not fount");
            result.setLastName(teacher.getLastName());
            result.setFirstName(teacher.getFirstName());
            result.setMiddleName(teacher.getMiddleName());
        } else {
            result = teacherRepository.save(new Teacher(teacher.getLastName(),teacher.getFirstName(),teacher.getMiddleName()));
        }

        return result;
    }

    @Transactional
    public TeacherSubject saveTeacherSubject(long teacherId, long subjectId){
        Teacher teacher = teacherRepository.findById(teacherId);
        notNull(teacher, "teacher by id=" + teacherId + " is not fount");

        Subject subject = subjectRepository.findById(subjectId);
        notNull(subject, "subject by id=" + subjectId + " is not fount");

        List<Subject> subjects = teacher.getSubjects();
        if(subjects.contains(subject))
            throw new ConflictSaveEntityException("subject already exist");

        subjects.add(subject);
        return new TeacherSubject(teacherId,subjectId);
    }

    @Transactional
    public void deleteTeacher(long id){
        Teacher teacher = teacherRepository.findById(id);
        notNull(teacher, "teacher by id=" + id + " is not fount");
        teacherRepository.delete(teacher);
    }

    @Transactional
    public void deleteTeacherSubject(long teacherId, long subjectId){
        Teacher teacher = teacherRepository.findById(teacherId);
        notNull(teacher, "teacher by id=" + teacherId + " is not fount");

        Subject subject = subjectRepository.findById(subjectId);
        notNull(subject, "subject by id=" + subjectId + " is not fount");

        teacher.getSubjects().remove(subject);
    }
}

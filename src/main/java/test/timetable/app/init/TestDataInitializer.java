package test.timetable.app.init;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import test.timetable.app.model.Subject;
import test.timetable.app.model.Teacher;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestDataInitializer {

    @Autowired
    private EntityManagerFactory entityManagerFactory;


    public void init() throws Exception {

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        List<Teacher> teachers = new ArrayList<>();
        List<Subject> subjects = new ArrayList<>();

        subjects.add(new Subject("����������"));
        subjects.add(new Subject("�������"));
        subjects.add(new Subject("���������"));

        teachers.add(new Teacher("������","����","��������",subjects));
        teachers.add(new Teacher("������","����","��������"));

        subjects.stream().forEach(session::persist);
        teachers.stream().forEach(session::persist);

        transaction.commit();
    }
}

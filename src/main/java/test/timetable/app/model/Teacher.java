package test.timetable.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TEACHERS")
@NamedQueries({
        @NamedQuery(
                name = Teacher.FIND_ALL,
                query = "select t from Teacher t"
        )
})
public class Teacher extends BaseEntity{

    public static final String FIND_ALL = "teacher.findAll";

    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @Column(name = "MIDDLE_NAME", nullable = false)
    private String middleName;

    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;

    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="TEACHER_SUBJECTS",
            joinColumns = {@JoinColumn(name="teacher_id", referencedColumnName="id")},
            inverseJoinColumns = {@JoinColumn(name="subject_id", referencedColumnName="id")}
    )
    private List<Subject> subjects;

    public Teacher() {
    }

    public Teacher(String lastName, String firstName, String middleName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public Teacher(String lastName, String firstName, String middleName, List<Subject> subjects){
        this(lastName,firstName,middleName);
        this.subjects = subjects;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}

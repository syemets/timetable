package test.timetable.app.model;

public class ErrorMessage {

    private int status;
    private String desc;

    public ErrorMessage() {
    }

    public ErrorMessage(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

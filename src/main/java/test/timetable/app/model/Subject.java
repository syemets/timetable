package test.timetable.app.model;

import javax.persistence.*;

@Entity
@Table(name = "SUBJECTS")
@NamedQueries({
        @NamedQuery(
                name = Subject.FIND_ALL,
                query = "select s from Subject s"
        )
})
public class Subject extends BaseEntity{

    public static final String FIND_ALL = "subject.findAll";

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    public Subject() {
    }

    public Subject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

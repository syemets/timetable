package test.timetable.app.model;

public class TeacherSubject {

    private long teacherId;

    private long subjectId;

    public TeacherSubject() {
    }

    public TeacherSubject(long teacherId, long subjectId) {
        this.teacherId = teacherId;
        this.subjectId = subjectId;
    }

    public long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(long teacherId) {
        this.teacherId = teacherId;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }
}

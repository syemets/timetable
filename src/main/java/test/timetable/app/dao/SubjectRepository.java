package test.timetable.app.dao;

import org.springframework.stereotype.Repository;
import test.timetable.app.model.Subject;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class SubjectRepository {

    @PersistenceContext
    private EntityManager em;

    public List<Subject> findAll(){
       return em.createNamedQuery(Subject.FIND_ALL, Subject.class).getResultList();
    }

    public Subject findById(long id){
        return em.find(Subject.class,id);
    }

    public Subject save(Subject subject) {
        return em.merge(subject);
    }

    public void delete(Subject subject) {
        em.remove(subject);
    }
}

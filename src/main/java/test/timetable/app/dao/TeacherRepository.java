package test.timetable.app.dao;

import org.springframework.stereotype.Repository;
import test.timetable.app.model.Teacher;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TeacherRepository {

    @PersistenceContext
    private EntityManager em;

    public List<Teacher> findAll(){
       return em.createNamedQuery(Teacher.FIND_ALL, Teacher.class).getResultList();
    }

    public Teacher findById(long id){
        return em.find(Teacher.class,id);
    }

    public Teacher save(Teacher teacher) {
        return em.merge(teacher);
    }

    public void delete(Teacher teacher) {
        em.remove(teacher);
    }
}
